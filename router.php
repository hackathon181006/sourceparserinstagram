<?php
error_reporting(E_ALL); 
ini_set("display_errors", 1); 
//header('Content-Type: application/json');
//before install:
// sudo apt-get install php-curl
// sudo apt-get install php-mbstring
// sudo apt-get install php-gd

set_time_limit(0);
date_default_timezone_set('UTC');

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/config.php';
//test id 8676270054
// http://localhost:8123/get-user?uid=8676270054
// http://localhost:8123/search-user?query=Завразина%20Татьяна

if (php_sapi_name() == 'cli-server') {
	\InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;

	$request_uri = strtok($_SERVER["REQUEST_URI"], '?');
	switch ($request_uri) {
		case '/get-user':						
			$ig = new \InstagramAPI\Instagram($config['debug'], $config['truncatedDebug']);

			try {
			    $ig->login($config['username'], $config['password']);
			} catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}    
			
			try {
				$userInfo = $ig->people->getInfoById($_REQUEST['uid']);
				echo json_encode($userInfo->getUser());
			}
			catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}
			break;
		case '/get-by-username':						
			$ig = new \InstagramAPI\Instagram($config['debug'], $config['truncatedDebug']);

			try {
			    $ig->login($config['username'], $config['password']);
			} catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}    
			
			try {
				$userId = $ig->people->getUserIdForName($_REQUEST['username']);
				$userInfo = $ig->people->getInfoById($userId);
				echo json_encode($userInfo->getUser());
			}
			catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}
			break;	
		case '/search-user':
			$ig = new \InstagramAPI\Instagram($config['debug'], $config['truncatedDebug']);

			try {
			    $ig->login($config['username'], $config['password']);
			} catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}    

			try {
			    $query = $_REQUEST['query'];
			    $users = array();
			    $rankToken = null;
			    $excludeList = [];

			    do {
			        $response = $ig->people->search($query, $excludeList, $rankToken);
			        //echo json_encode($response->getUsers());
			        
			        foreach ($response->getUsers() as $user) {
			        	//$userInfo = $user->getInfoById($user->getPk());
			            array_push($users, $user);

			            //$location = $item->getLocation();
			            //$excludeList[] = $location->getFacebookPlacesId();
			            //printf("%s (%.3f, %.3f)\n", $item->getTitle(), $location->getLat(), $location->getLng());
			        }
			        
			        
			        $rankToken = $response->getRankToken();
			        //echo "Sleeping for 5s...\n";

			        break;
			        sleep(5);
			    } while ($response->getHasMore());
			    echo json_encode($users);
			} catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			}

			break;			
		case '/get-by-phone':
			$ig = new \InstagramAPI\Instagram($config['debug'], $config['truncatedDebug']);

			try {
			    $ig->login($config['username'], $config['password']);
			} catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}    
			
			try {
				$ig->people->unlinkAddressBook();
				$contact = (object)array('first_name' => $_REQUEST['first_name'], 'phone_numbers' => array($_REQUEST['phone']), 'email_addresses' => array()); //+79134620492

				//array({"first_name":"Tom","phone_numbers":[""],"email_addresses":["Tom@web.de"]})
				$response = $ig->people->linkAddressBook(array($contact)); //$_REQUEST['phone']

				echo json_encode($response);
			}
			catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}
			break;	
		case '/send-direct':
			$ig = new \InstagramAPI\Instagram($config['debug'], $config['truncatedDebug']);

			try {
			    $ig->login($config['username'], $config['password']);
			} catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}    
			
			try {
				$userId = $ig->people->getUserIdForName($_REQUEST['username']);
				//echo $userId;
				$sendTextResult = $ig->direct->sendText(array('users' => array($userId)), $_REQUEST['message']);
				echo json_encode($sendTextResult);
			}
			catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}

			break;
		case '/get-inbox':

			$ig = new \InstagramAPI\Instagram($config['debug'], $config['truncatedDebug']);

			try {
			    $ig->login($config['username'], $config['password']);
			} catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}    
			
			try {				
				$getInboxResult = $ig->direct->getInbox();
				echo json_encode($getInboxResult->getInbox()->getThreads());
			}
			catch (\Exception $e) {
			    echo 'Something went wrong: '.$e->getMessage()."\n";
			    exit(0);
			}		
			break;		
		default:
			return false;
			break;
	}

}

?>